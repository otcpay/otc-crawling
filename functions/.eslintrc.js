module.exports = {
  root: true,
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    'eslint:recommended',
    'google',
  ],
  rules: {
    quotes: ['error', 'single'],
    indent: ['error', 2],
    'quote-props': 0,
    'max-len': ['error', 210],
  },
};
