const admin = require('firebase-admin');
const functions = require('firebase-functions');

const crawl = require('./src/crawl').default;
const clean = require('./src/clean').default;
const adStatus = require('./src/status').adStatus;
const buySellRecordStatus = require('./src/status').buySellRecordStatus;
const invoicePaymentStatus = require('./src/status').invoicePaymentStatus;
const onSettlementCreateTrigger = require('./src/trigger').onSettlementCreateTrigger;
const onAdvertisementCreateTrigger = require('./src/trigger').onAdvertisementCreateTrigger;
const onBuySellRecordCreateTrigger = require('./src/trigger').onBuySellRecordCreateTrigger;

admin.initializeApp();

const runtimeOpts = {
  timeoutSeconds: 500,
  memory: '512MB',
};

exports.scheduledFxCrawl = functions.runWith(runtimeOpts).pubsub.schedule('every 24 hours').timeZone('Asia/Hong_Kong').onRun((context) => {
  const date = new Date().toLocaleDateString('en-GB', {timeZone: 'Asia/Hong_Kong'}).replace(/\//g, '-');
  functions.logger.info(`Running fx rate schdeule crawling on ${date}`, {structuredData: true});
  return crawl(false);
});

exports.crawlFxPrice = functions.runWith(runtimeOpts).https.onRequest((request, response) => {
  const date = new Date().toLocaleDateString('en-GB', {timeZone: 'Asia/Hong_Kong'}).replace(/\//g, '-');
  functions.logger.info(`Running fx rate crawling on ${date}`, {structuredData: true});
  crawl(true).then(() => {
    response.send({message: 'Success'});
  });
});

exports.cleanUpFxPrice = functions.runWith(runtimeOpts).https.onRequest((request, response) => {
  const date = new Date().toLocaleDateString('en-GB', {timeZone: 'Asia/Hong_Kong'}).replace(/\//g, '-');
  functions.logger.info(`Running fx rate cleaning on ${date}`, {structuredData: true});
  clean().then(() => {
    response.send({message: 'Success'});
  });
});

exports.scheduledAdStatusUpdate = functions.runWith(runtimeOpts).pubsub.schedule('every 24 hours').timeZone('Asia/Hong_Kong').onRun((context) => {
  functions.logger.info('Running status update on Advertisements', {structuredData: true});
  return adStatus();
});

exports.scheduledBuSellRecordStatusUpdate = functions.runWith(runtimeOpts).pubsub.schedule('every 24 hours').timeZone('Asia/Hong_Kong').onRun((context) => {
  functions.logger.info('Running status update on BuySellRecords', {structuredData: true});
  return buySellRecordStatus();
});

exports.scheduledInvoicePaymentUpdate = functions.runWith(runtimeOpts).pubsub.schedule('every 24 hours').timeZone('Asia/Hong_Kong').onRun((context) => {
  functions.logger.info('Running status update on InvoicePayments', {structuredData: true});
  return invoicePaymentStatus();
});

exports.onSettlementCreateTrigger = functions.firestore.document('Settlements/{settlementId}').onCreate(onSettlementCreateTrigger);
exports.onAdvertisementCreateTrigger = functions.firestore.document('Advertisements/{advertisementId}').onCreate(onAdvertisementCreateTrigger);
exports.onBuySellRecordCreateTrigger = functions.firestore.document('BuySellRecords/{buySellRecordId}').onCreate(onBuySellRecordCreateTrigger);
