const admin = require('firebase-admin');

const cleanup = async () => {
  try {
    const firestore = admin.firestore();
    firestore.settings({ignoreUndefinedProperties: true});

    const date = new Date();
    date.setHours(0, 0, 0, 0);
    const dateString = date.toISOString();
    const existingTTRate = await firestore.collection('ttrate').where('time', '>=', dateString).get();
    const ttresults = await Promise.all(existingTTRate.docs.map((doc) => doc.ref.delete()));
    const existingCm = await firestore.collection('coinmill').where('time', '>=', dateString).get();
    const cmresults = await Promise.all(existingCm.docs.map((doc) => doc.ref.delete()));
    const existingTw = await firestore.collection('transferwise').where('time', '>=', dateString).get();
    const twresults = await Promise.all(existingTw.docs.map((doc) => doc.ref.delete()));
    const existingWe = await firestore.collection('westernunion').where('time', '>=', dateString).get();
    const wuresults = await Promise.all(existingWe.docs.map((doc) => doc.ref.delete()));
    const existingMg = await firestore.collection('moneygram').where('time', '>=', dateString).get();
    const mgresults = await Promise.all(existingMg.docs.map((doc) => doc.ref.delete()));
    const existingXe = await firestore.collection('xe').where('time', '>=', dateString).get();
    const xeresults = await Promise.all(existingXe.docs.map((doc) => doc.ref.delete()));
    const existingXrate = await firestore.collection('xrate').where('time', '>=', dateString).get();
    const xrresults = await Promise.all(existingXrate.docs.map((doc) => doc.ref.delete()));

    console.log(`Number of ttrate record deleted: ${ttresults.length.toString()}`);
    console.log(`Number of transferwise record deleted: ${twresults.length.toString()}`);
    console.log(`Number of westernunion record deleted: ${wuresults.length.toString()}`);
    console.log(`Number of moneygram record deleted: ${mgresults.length.toString()}`);
    console.log(`Number of coinmill record deleted: ${cmresults.length.toString()}`);
    console.log(`Number of xe record deleted: ${xeresults.length.toString()}`);
    console.log(`Number of xrate record deleted: ${xrresults.length.toString()}`);
  } catch (error) {
    console.error('Unknown error', error);
  }
};

exports.default = cleanup;
