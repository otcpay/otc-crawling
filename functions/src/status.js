const admin = require('firebase-admin');

const threeDayAgoInMillis = 3 * 24 * 60 * 60 * 1000;

const adStatus = async () => {
  const ids = [];
  const firestore = admin.firestore();

  const threeDayAgo = Date.now() - threeDayAgoInMillis;
  const colRef = firestore.collection('Advertisements');
  const querySnapshot = await colRef.where('status', '!=', 'Expired').get();
  querySnapshot.forEach((doc) => {
    const data = doc.data();
    if (data.updatedDate <= threeDayAgo) {
      ids.push(doc.id);
    }
  });
  console.log(`Updating ${ids.length} Advertisements`);

  await Promise.all(ids.map((id) => colRef.doc(id).update({status: 'Expired'})));
};

const buySellRecordStatus = async () => {
  const ids = [];
  const firestore = admin.firestore();

  const threeDayAgo = Date.now() - threeDayAgoInMillis;
  const colRef = firestore.collection('BuySellRecords');
  const querySnapshot = await colRef.where('status', '==', 'Processing').get();
  querySnapshot.forEach((doc) => {
    const data = doc.data();
    if (data.updatedDate <= threeDayAgo) {
      ids.push(doc.id);
    }
  });
  console.log(`Updating ${ids.length} BuySellRecords`);

  await Promise.all(ids.map((id) => colRef.doc(id).update({status: 'Cancelled'})));
};

const invoicePaymentStatus = async () => {
  const ids = [];
  const firestore = admin.firestore();

  const threeDayAgo = Date.now() - threeDayAgoInMillis;
  const colRef = firestore.collection('InvoicePayments');
  const querySnapshot = await colRef.where('status', '==', 'Processing').get();
  querySnapshot.forEach((doc) => {
    const data = doc.data();
    if (data.updatedDate <= threeDayAgo) {
      ids.push(doc.id);
    }
  });
  console.log(`Updating ${ids.length} InvoicePayments`);

  await Promise.all(ids.map((id) => colRef.doc(id).update({status: 'Expired'})));
};

exports.adStatus = adStatus;
exports.buySellRecordStatus = buySellRecordStatus;
exports.invoicePaymentStatus = invoicePaymentStatus;
