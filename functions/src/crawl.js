const admin = require('firebase-admin');
const ttrateCrawl = require('./source/ttrate');
const twCrawl = require('./source/transferwise');
const weCrawl = require('./source/westernunion');
const mgCrawl = require('./source/moneygram');
const cmCrawl = require('./source/otc');
const xeCrawl = require('./source/xe');
const xrateCrawl = require('./source/xrate');

const ttRateregionList = ['hk', 'tw', 'cn', 'mo', 'sg'];
const transferwiseRegionList = ['cn', 'hk', 'us', 'kr', 'jp', 'tw', 'mo', 'sg', 'eu', 'th', 'gb', 'my', 'vn', 'ph'];
const currencyList = ['CNY', 'HKD', 'USD', 'KRW', 'JPY', 'TWD', 'MOP', 'SGD', 'EUR', 'THB', 'GBP', 'MYR', 'VND', 'PHP'];
const mgCountryList = ['USA', 'HKG', 'CHN', 'VNM', 'KOR', 'JPN', 'TWN', 'MAC', 'SGP', 'THA', 'GBR', 'MYS', 'PHL'];
const mapping = {
  hk: 'HKD',
  tw: 'TWD',
  cn: 'CNY',
  mo: 'MOP',
  sg: 'SGD',
  us: 'USD',
  kr: 'KRW',
  jp: 'JPY',
  eu: 'EUR',
  th: 'THB',
  gb: 'GBP',
  my: 'MYR',
  vn: 'VND',
  ph: 'PHP',
};
const reverseMapping = {
  HKD: 'hk',
  TWD: 'tw',
  CNY: 'cn',
  MOP: 'mo',
  SGD: 'sg',
  USD: 'us',
  KRW: 'kr',
  JPY: 'jp',
  EUR: 'eu',
  THB: 'th',
  GBP: 'gb',
  MYR: 'my',
  VND: 'vn',
  PHP: 'ph',
};
const mgMapping = {
  USA: 'USD',
  HKG: 'HKD',
  CHN: 'CNY',
  VNM: 'VND',
  KOR: 'KRW',
  JPN: 'JPY',
  TWN: 'TWD',
  MAC: 'MOP',
  SGP: 'SGD',
  THA: 'THB',
  GBR: 'GBP',
  MYS: 'MYR',
  PHL: 'PHP',
};

const crawl = async (rerun) => {
  try {
    const firestore = admin.firestore();
    firestore.settings({ignoreUndefinedProperties: true});

    if (rerun) {
      const date = new Date();
      date.setHours(0, 0, 0, 0);
      const dateString = date.toISOString();
      const existingTTRate = await firestore.collection('ttrate').where('time', '>=', dateString).get();
      const ttresults = await Promise.all(existingTTRate.docs.map((doc) => doc.ref.delete()));
      const existingCm = await firestore.collection('coinmill').where('time', '>=', dateString).get();
      const cmresults = await Promise.all(existingCm.docs.map((doc) => doc.ref.delete()));
      const existingTw = await firestore.collection('transferwise').where('time', '>=', dateString).get();
      const twresults = await Promise.all(existingTw.docs.map((doc) => doc.ref.delete()));
      const existingWe = await firestore.collection('westernunion').where('time', '>=', dateString).get();
      const wuresults = await Promise.all(existingWe.docs.map((doc) => doc.ref.delete()));
      const existingMg = await firestore.collection('moneygram').where('time', '>=', dateString).get();
      const mgresults = await Promise.all(existingMg.docs.map((doc) => doc.ref.delete()));
      const existingXe = await firestore.collection('xe').where('time', '>=', dateString).get();
      const xeresults = await Promise.all(existingXe.docs.map((doc) => doc.ref.delete()));
      const existingXrate = await firestore.collection('xrate').where('time', '>=', dateString).get();
      const xrresults = await Promise.all(existingXrate.docs.map((doc) => doc.ref.delete()));

      console.log(`Number of ttrate record deleted: ${ttresults.length.toString()}`);
      console.log(`Number of transferwise record deleted: ${twresults.length.toString()}`);
      console.log(`Number of westernunion record deleted: ${wuresults.length.toString()}`);
      console.log(`Number of moneygram record deleted: ${mgresults.length.toString()}`);
      console.log(`Number of coinmill record deleted: ${cmresults.length.toString()}`);
      console.log(`Number of xe record deleted: ${xeresults.length.toString()}`);
      console.log(`Number of xrate record deleted: ${xrresults.length.toString()}`);
    }

    const ttresults = await ttrateCrawl(ttRateregionList, currencyList, mapping);
    const twresults = await twCrawl(transferwiseRegionList, mapping);
    const wuresults = await weCrawl(currencyList, reverseMapping);
    const cmresults = await cmCrawl(currencyList);
    const mgresults = await mgCrawl(mgCountryList, mgMapping);
    const xeresults = await xeCrawl(currencyList, reverseMapping);
    const xrresults = await xrateCrawl(currencyList, reverseMapping);

    console.log(`Number of ttrate record created: ${ttresults.length.toString()}`);
    console.log(`Number of transferwise record created: ${twresults.length.toString()}`);
    console.log(`Number of westernunion record created: ${wuresults.length.toString()}`);
    console.log(`Number of moneygram record created: ${mgresults.length.toString()}`);
    console.log(`Number of coinmill record created: ${cmresults.length.toString()}`);
    console.log(`Number of xe record created: ${xeresults.length.toString()}`);
    console.log(`Number of xrate record created: ${xrresults.length.toString()}`);

    await Promise.all(cmresults.map((result) => firestore.collection('coinmill').add(result)));
    await Promise.all(ttresults.map((result) => firestore.collection('ttrate').add(result)));
    await Promise.all(twresults.map((result) => firestore.collection('transferwise').add(result)));
    await Promise.all(wuresults.map((result) => firestore.collection('westernunion').add(result)));
    await Promise.all(mgresults.map((result) => firestore.collection('moneygram').add(result)));
    await Promise.all(xeresults.map((result) => firestore.collection('xe').add(result)));
    await Promise.all(xrresults.map((result) => firestore.collection('xrate').add(result)));
  } catch (error) {
    console.error('Unknown error', error);
  }
};

exports.default = crawl;
