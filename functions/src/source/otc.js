const request = require('request');
const JSSoup = require('jssoup').default;

const asyncRequest = (currency) => new Promise((resolve, reject) => {
  request(`https://coinmill.com/${currency}_USDT.html`, (error, response, body) => {
    if (error) {
      console.log('Error sending request to coinmill', error);
      resolve({});
    }

    resolve(body);
  });
});

const getOrder = (from, to) => {
  if (from === 'USDT' && to === 'HKD') {
    return 0;
  }

  if (from === 'HKD' && to === 'USDT') {
    return 1;
  }

  if (from === 'USDT' && to === 'USD') {
    return 2;
  }

  if (from === 'USD' && to === 'USDT') {
    return 3;
  }

  if (from === 'USDT' && to === 'CNY') {
    return 4;
  }

  return 5;
};

const crawlAllRate = async (currency) => {
  let cells = [];

  const body = await asyncRequest(currency);
  const soup = new JSSoup(body);
  const tables = soup.findAll('table', 'conversionchart');
  const rateRows = tables.map((table) => table.findAll('tr')[2]);
  rateRows.forEach((row) => {
    cells = cells.concat(row.findAll('td'));
  });

  const rates = cells.map((cell) => cell.getText().replace(/\n|,/g, ''));
  const buyRate = parseFloat(rates[1]) / parseFloat(rates[0]);
  const sellRate = parseFloat(rates[3]) / parseFloat(rates[2]);

  return [{
    source: 'CoinMill',
    sourceCurrency: 'USDT',
    targetCurrency: currency,
    rate: sellRate,
    type: 'SELL',
    time: new Date().toISOString(),
    order: getOrder(currency, 'USDT'),
  }, {
    source: 'CoinMill',
    sourceCurrency: currency,
    targetCurrency: 'USDT',
    rate: 1 / buyRate,
    type: 'BUY',
    time: new Date().toISOString(),
    order: getOrder('USDT', currency),
  }];
};

const crawl = async (currencies) => {
  let rateMap = [];

  const promises = currencies.map((c) => crawlAllRate(c));
  const results = await Promise.all(promises);
  for (const result of results) {
    rateMap = rateMap.concat(result);
  }

  return rateMap;
};

module.exports = crawl;

crawlAllRate('VND');
