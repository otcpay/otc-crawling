const request = require('request');

const asyncRequest = (sourceCurrency, targetCurrency) => new Promise((resolve, reject) => {
  request(`https://business.westernunion.com/api/converter/convert/1.00/${sourceCurrency}/${targetCurrency}`,
    (error, response, body) => {
      if (error) {
        console.log('Error sending request to westernunion', error);
        resolve({});
      }

      try {
        resolve(JSON.parse(body));
      } catch (e) {
        resolve({});
      }
    });
});

const crawl = async (currencyList, reverseMapping) => {
  const promises = [];
  for (const sourceCurrency of currencyList) {
    for (const targetCurrency of currencyList) {
      promises.push(asyncRequest(sourceCurrency, targetCurrency));
    }
  }

  const results = await Promise.all(promises);
  const convertedResult = results.filter((result) => {
    if (Object.keys(result).length == 0) return false;
    if (result.Error) return result.Error.length === 0;

    return true;
  }).map((result) => ({
    source: 'Western Union',
    sourceCountry: reverseMapping[result.FromCurrency],
    sourceCurrency: result.FromCurrency,
    targetCountry: reverseMapping[result.ToCurrency],
    targetCurrency: result.ToCurrency,
    rate: result.Rate,
    fee: 0,
    time: new Date().toISOString(),
  }));

  return convertedResult.filter((r) => r.sourceCountry && r.targetCountry);
};

module.exports = crawl;
