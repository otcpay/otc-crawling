const request = require('request');
const JSSoup = require('jssoup').default;

const asyncRequest = (sourceCurrency, targetCurrency) => new Promise((resolve, reject) => {
  request(`https://www.x-rates.com/calculator/?from=${sourceCurrency}&to=${targetCurrency}&amount=1`,
    (error, response, body) => {
      if (error) {
        console.log('Error sending request to xe', error);
        resolve({});
      }

      try {
        resolve(body);
      } catch (e) {
        resolve('');
      }
    });
});

const crawl = async (currencyList, reverseMapping) => {
  const rateMap = [];
  const promises = [];
  for (const sourceCurrency of currencyList) {
    for (const targetCurrency of currencyList) {
      if (sourceCurrency != targetCurrency) {
        promises.push(asyncRequest(sourceCurrency, targetCurrency));
      }
    }
  }

  const results = await Promise.all(promises);
  const convertedResult = results.filter((result) => result.length != 0);
  for (let j = 0; j < convertedResult.length; j++) {
    const body = convertedResult[j];
    const soup = new JSSoup(body);
    if (soup) {
      const fxRateDiv = soup.findAll('div', 'ccOutputBx');
      if (fxRateDiv) {
        const fxRateResultSpan = soup.find('span', 'ccOutputRslt');
        if (fxRateResultSpan) {
          const fxRateResult = fxRateResultSpan.getText();
          const from = soup.find('span', 'ccOutputTxt').getText().replace(/[^a-zA-Z]/g, '');
          const to = fxRateResult.replace(/[^a-zA-Z]/g, '');
          const rate = fxRateResult.replace(/[^0-9.]/g, '');

          rateMap.push({
            source: 'X-Rates',
            sourceCountry: reverseMapping[from],
            sourceCurrency: from,
            targetCountry: reverseMapping[to],
            targetCurrency: to,
            rate: parseFloat(rate),
            fee: 0,
            time: new Date().toISOString(),
          });
        }
      }
    }
  }

  return rateMap.filter((r) => r.sourceCountry && r.targetCountry);
};

module.exports = crawl;
