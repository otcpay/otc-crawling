const request = require('request');
const JSSoup = require('jssoup').default;

const asyncRequest = (sourceCurrency, targetCurrency) => new Promise((resolve, reject) => {
  request(`https://www.xe.com/zh-HK/currencyconverter/convert/?Amount=1&From=${sourceCurrency}&To=${targetCurrency}`,
    (error, response, body) => {
      if (error) {
        console.log('Error sending request to xe', error);
        resolve({});
      }

      try {
        resolve(body);
      } catch (e) {
        resolve('');
      }
    });
});

const crawl = async (currencyList, reverseMapping) => {
  const rateMap = [];
  const promises = [];
  for (const sourceCurrency of currencyList) {
    if (sourceCurrency != 'HKD') {
      promises.push(asyncRequest(sourceCurrency, 'HKD'));
    }
  }

  const results = await Promise.all(promises);
  const convertedResult = results.filter((result) => result.length != 0);
  for (let j = 0; j < convertedResult.length; j++) {
    const body = convertedResult[j];
    const soup = new JSSoup(body);
    if (soup) {
      const fxTableDiv = soup.findAll('div', 'carousel__Slide-zkiqgn-1');
      if (fxTableDiv) {
        const rateCell = fxTableDiv[0].findAll('td');
        const reverseRateCell = fxTableDiv[1].findAll('td');
        const from = rateCell[1].getText().replace(/[^a-zA-Z]/g, '');
        const to = reverseRateCell[1].getText().replace(/[^a-zA-Z]/g, '');
        const rate = rateCell[1].getText().replace(/[^0-9.]/g, '');
        const reverseRate = reverseRateCell[1].getText().replace(/[^0-9.]/g, '');
        rateMap.push({
          source: 'XE',
          sourceCountry: reverseMapping[from],
          sourceCurrency: from,
          targetCountry: reverseMapping[to],
          targetCurrency: to,
          rate: parseFloat(reverseRate),
          fee: 0,
          time: new Date().toISOString(),
        });
        rateMap.push({
          source: 'XE',
          sourceCountry: reverseMapping[to],
          sourceCurrency: to,
          targetCountry: reverseMapping[from],
          targetCurrency: from,
          rate: parseFloat(rate),
          fee: 0,
          time: new Date().toISOString(),
        });
      }
    }
  }

  return rateMap;
};

module.exports = crawl;
