const request = require('request');
const JSSoup = require('jssoup').default;

const asyncRequest = (region, currency) => new Promise((resolve, reject) => {
  request(`https://${region}.ttrate.com/en_us/?c=${currency}`, (error, response, body) => {
    if (error) {
      console.log('Error sending request to moneygram', error);
      resolve('<div></div>');
    }

    resolve(body);
  });
});

const getDisplayOrder = (name) => {
  if (name === 'HSBC') return 0;
  if (name === 'Hang Seng') return 1;
  if (name === 'BOC') return 2;
  if (name === 'ICBC') return 3;
  if (name === 'Berlin') return 4;
  if (name === 'Travelex (Reserve)') return 5;

  return 99;
};

const crawlAllRateOfRegion = async (region, currencyList, mapping) => {
  const rateMap = [];

  let bodys = await Promise.all(currencyList.map((currency) => asyncRequest(region, currency)));
  for (let j = 0; j < bodys.length; j++) {
    const body = bodys[j];
    const soup = new JSSoup(body);
    const fxTableDiv = soup.find('div', 'rate_table_container');
    if (fxTableDiv) {
      const names = soup.findAll('td', 'col_bank').map((row) => row.find('span').getText());
      const ttBuy = soup.findAll('td', 'col_tt_buy').map((row) => row.getText());
      const ttSell = soup.findAll('td', 'col_tt_sell').map((row) => row.getText());
      const cashBuy = soup.findAll('td', 'col_note_buy').map((row) => row.getText());
      const cashSell = soup.findAll('td', 'col_note_sell').map((row) => row.getText());

      for (let i = 0; i < names.length; i++) {
        rateMap.push({
          source: names[i],
          sourceCountry: region,
          sourceCurrency: currencyList[j],
          targetCountry: region,
          targetCurrency: mapping[region],
          ttBuy: ttBuy[i] === '-' ? '0' : ttBuy[i],
          ttSell: ttSell[i] === '-' ? '0' : ttSell[i],
          cashBuy: cashBuy[i] === '-' ? '0' : cashBuy[i],
          cashSell: cashSell[i] === '-' ? '0' : cashSell[i],
          time: new Date().toISOString(),
          order: getDisplayOrder(names[i]),
        });
      }
    }
  }

  bodys = await Promise.all(currencyList.map((currency) => asyncRequest(region, `${currency}&f=1`)));
  for (let j = 0; j < bodys.length; j++) {
    const body = bodys[j];
    const soup = new JSSoup(body);
    const fxTableDiv = soup.find('div', 'rate_table_container');
    if (fxTableDiv) {
      const names = soup.findAll('td', 'col_bank').map((row) => row.find('span').getText());
      const ttBuy = soup.findAll('td', 'col_tt_buy').map((row) => row.getText());
      const ttSell = soup.findAll('td', 'col_tt_sell').map((row) => row.getText());
      const cashBuy = soup.findAll('td', 'col_note_buy').map((row) => row.getText());
      const cashSell = soup.findAll('td', 'col_note_sell').map((row) => row.getText());

      for (let i = 0; i < names.length; i++) {
        rateMap.push({
          source: names[i],
          sourceCountry: region,
          sourceCurrency: mapping[region],
          targetCountry: region,
          targetCurrency: currencyList[j],
          ttBuy: ttBuy[i] === '-' ? '0' : ttBuy[i],
          ttSell: ttSell[i] === '-' ? '0' : ttSell[i],
          cashBuy: cashBuy[i] === '-' ? '0' : cashBuy[i],
          cashSell: cashSell[i] === '-' ? '0' : cashSell[i],
          time: new Date().toISOString(),
          order: getDisplayOrder(names[i]),
        });
      }
    }
  }

  return rateMap;
};

const crawl = async (regionList, currencyList, mapping) => {
  let rateMap = [];

  const promises = regionList.map((region) => crawlAllRateOfRegion(region, currencyList, mapping));
  const results = await Promise.all(promises);
  for (const result of results) {
    rateMap = rateMap.concat(result);
  }

  return rateMap;
};

module.exports = crawl;
