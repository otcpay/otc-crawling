const request = require('request');

const asyncRequest = (senderCountry, senderCurrency, targetCountry, targetCurrency) => new Promise((resolve, reject) => {
  request({
    url: `https://consumerapi.moneygram.com/services/capi/api/v1/sendMoney/feeLookup?senderCountry=${senderCountry}&senderCurrency=${senderCurrency}&receiveCountry=${targetCountry}&sendAmount=1`,
    headers: {
      clientKey: 'Basic V0VCX2VkYjI0ZDc5LTA0ODItNDdlMi1hNmQ2LTc4ZGY5YzI4MmM0ZTo1MTNlMTEyOS0yZTJmLTRlYmUtYjkwMi02YTVkMGViMDNjZjc=',
    },
  },
  (error, response, body) => {
    if (error) {
      console.log('Error sending request to moneygram', error);
      resolve({});
    }

    try {
      const rsp = JSON.parse(body);
      if (rsp.error == null && targetCurrency === rsp.paymentOptions[0].receiveGroups[0].receiveOptions[0].receiveCurrency) {
        resolve({
          senderCountry,
          senderCurrency,
          targetCountry,
          targetCurrency,
          rate: rsp.paymentOptions[0].receiveGroups[0].receiveOptions[0].exchangeRate,
          fee: rsp.paymentOptions[0].receiveGroups[0].receiveOptions[0].sendFees,
        });
      } else {
        resolve({});
      }
    } catch (e) {
      resolve({});
    }
  });
});

const crawl = async (countryList, mapping) => {
  const promises = [];
  for (const senderCountry of countryList) {
    for (const targetCountry of countryList) {
      promises.push(asyncRequest(senderCountry, mapping[senderCountry], targetCountry, mapping[targetCountry]));
    }
  }

  const results = await Promise.all(promises);
  const convertedResult = results.filter((result) => {
    return Object.keys(result).length > 0;
  }).map((result) => ({
    source: 'MoneyGram',
    sourceCountry: result.senderCountry,
    sourceCurrency: result.senderCurrency,
    targetCountry: result.targetCountry,
    targetCurrency: result.targetCurrency,
    rate: result.rate,
    fee: result.fee,
    time: new Date().toISOString(),
  }));
  return convertedResult;
};

module.exports = crawl;
