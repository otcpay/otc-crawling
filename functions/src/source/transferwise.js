const request = require('request');

const asyncRequest = (sourceCurrency, targetCurrency, sourceCountry, targetCountry) => new Promise((resolve, reject) => {
  request(`https://transferwise.com/gateway/v3/comparisons?sendAmount=1&sourceCurrency=${sourceCurrency}&targetCurrency=${targetCurrency}&targetCountry=${sourceCountry}&sourceCountry=${targetCountry}`,
    (error, response, body) => {
      if (error) {
        console.log('Error sending request to transferwise', error);
        resolve({});
      }

      try {
        resolve(JSON.parse(body));
      } catch (e) {
        resolve({});
      }
    });
});

const crawlCurrency = async (sourceRegion, targetRegion, targetCurrency, mapping) => {
  const rateMap = [];

  const response = await asyncRequest(mapping[sourceRegion], targetCurrency, sourceRegion.toUpperCase(), targetRegion.toUpperCase());
  if (response.providers && response.providers.length > 0) {
    response.providers.forEach((provider) => {
      rateMap.push({
        source: 'TransferWise',
        sourceCountry: sourceRegion,
        sourceCurrency: mapping[sourceRegion],
        targetCountry: targetRegion,
        targetCurrency: targetCurrency,
        rate: provider.quotes[0].rate,
        fee: provider.quotes[0].fee,
        time: new Date().toISOString(),
      });
    });
  }

  return rateMap;
};

const crawlTargetRegion = async (sourceRegion, targetRegion, mapping) => {
  let rateMap = [];

  const promises = Object.keys(mapping).map((key) => crawlCurrency(sourceRegion, targetRegion, mapping[key], mapping));
  const results = await Promise.all(promises);
  for (const result of results) {
    rateMap = rateMap.concat(result);
  }

  return rateMap;
};

const crawlSourceRegion = async (sourceRegion, regionList, mapping) => {
  let rateMap = [];

  const promises = regionList.map((region) => crawlTargetRegion(sourceRegion, region, mapping));
  const results = await Promise.all(promises);
  for (const result of results) {
    rateMap = rateMap.concat(result);
  }

  return rateMap;
};

const crawl = async (regionList, mapping) => {
  let rateMap = [];

  const promises = regionList.map((region) => crawlSourceRegion(region, regionList, mapping));
  const results = await Promise.all(promises);
  for (const result of results) {
    rateMap = rateMap.concat(result);
  }

  return rateMap;
};

module.exports = crawl;
