const admin = require('firebase-admin');

const onSettlementCreateTrigger = async (snap, context) => {
  const firestore = admin.firestore();

  const newValue = snap.data();

  const detailRef = firestore.collection('Settlements').doc('Detail');
  const doc = await detailRef.get();
  let detail = {};
  if (doc.exists) {
    detail = doc.data();
    if (newValue.type === 'OTC') {
      const newCount = detail.OTC + 1;
      detail.OTC = newCount;
    } else if (newValue.type === 'B2B') {
      const newCount = detail.B2B + 1;
      detail.B2B = newCount;
    } else if (newValue.type === 'B2C') {
      const newCount = detail.B2C + 1;
      detail.B2C = newCount;
    } else if (newValue.type === 'DIRECT') {
      const newCount = detail.DIRECT + 1;
      detail.DIRECT = newCount;
    }
  }

  await detailRef.set(detail);
};

const onBuySellRecordCreateTrigger = async (snap, context) => {
  const firestore = admin.firestore();

  const newValue = snap.data();

  const detailRef = firestore.collection('BuySellRecords').doc('Detail');
  const doc = await detailRef.get();
  let detail = {};
  if (doc.exists) {
    detail = doc.data();
    if (newValue.type === 'BUY') {
      const newCount = detail.type.BUY + 1;
      detail.type.BUY = newCount;
    } else if (newValue.type === 'SELL') {
      const newCount = detail.type.SELL + 1;
      detail.type.SELL = newCount;
    }

    if (newValue.crytocurrency === 'USDT') {
      const newCount = detail.currency.USDT + 1;
      detail.currency.USDT = newCount;
    } else if (newValue.crytocurrency === 'BTC') {
      const newCount = detail.currency.BTC + 1;
      detail.currency.BTC = newCount;
    } else if (newValue.crytocurrency === 'ETH') {
      const newCount = detail.currency.ETH + 1;
      detail.currency.ETH = newCount;
    } else if (newValue.crytocurrency === 'XRP') {
      const newCount = detail.currency.XRP + 1;
      detail.currency.XRP = newCount;
    } else if (newValue.crytocurrency === 'USDC') {
      const newCount = detail.currency.USDC + 1;
      detail.currency.USDC = newCount;
    } else if (newValue.crytocurrency === 'BUSD') {
      const newCount = detail.currency.BUSD + 1;
      detail.currency.BUSD = newCount;
    }
  }

  await detailRef.set(detail);
};

const onAdvertisementCreateTrigger = async (snap, context) => {
  const firestore = admin.firestore();

  const newValue = snap.data();

  const detailRef = firestore.collection('Advertisements').doc('Detail');
  const doc = await detailRef.get();
  let detail = {};
  if (doc.exists) {
    detail = doc.data();
    if (newValue.type === 'BUY') {
      const newCount = detail.type.BUY + 1;
      detail.type.BUY = newCount;
    } else if (newValue.type === 'SELL') {
      const newCount = detail.type.SELL + 1;
      detail.type.SELL = newCount;
    }

    if (newValue.crytocurrency === 'USDT') {
      const newCount = detail.currency.USDT + 1;
      detail.currency.USDT = newCount;
    } else if (newValue.crytocurrency === 'BTC') {
      const newCount = detail.currency.BTC + 1;
      detail.currency.BTC = newCount;
    } else if (newValue.crytocurrency === 'ETH') {
      const newCount = detail.currency.ETH + 1;
      detail.currency.ETH = newCount;
    } else if (newValue.crytocurrency === 'XRP') {
      const newCount = detail.currency.XRP + 1;
      detail.currency.XRP = newCount;
    } else if (newValue.crytocurrency === 'USDC') {
      const newCount = detail.currency.USDC + 1;
      detail.currency.USDC = newCount;
    } else if (newValue.crytocurrency === 'BUSD') {
      const newCount = detail.currency.BUSD + 1;
      detail.currency.BUSD = newCount;
    }
  }

  await detailRef.set(detail);
};

exports.onSettlementCreateTrigger = onSettlementCreateTrigger;
exports.onBuySellRecordCreateTrigger = onBuySellRecordCreateTrigger;
exports.onAdvertisementCreateTrigger = onAdvertisementCreateTrigger;
